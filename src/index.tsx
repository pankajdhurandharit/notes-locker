import React from 'react';
import ReactDOM from 'react-dom';
import firebase from "firebase";
// Required for side-effects
import "firebase/firestore";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDwutKY7IlU34fTa2x6348ibosiQm4he6c",
  authDomain: "mydb-e5c7a.firebaseapp.com",
  projectId: "mydb-e5c7a",
  storageBucket: "mydb-e5c7a.appspot.com",
  messagingSenderId: "1021684071669",
  appId: "1:1021684071669:web:2ce32c9dadc9d7321619bc"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const fireDb = firebase.firestore();
export default fireDb;

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

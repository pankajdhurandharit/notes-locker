import React from 'react';
import './App.css';
import LoginPage from './ui/login-page';
import StickiesConatiner from './ui/stickys-cmpt/stickies-conatiner';


interface IProps {

}

interface IState {
  ui: JSX.Element

}

class App extends React.PureComponent<IProps, IState>{

  //Stuck Here
  state: IState = {
    ui: <LoginPage loginCB={() => {
      this.setState({
        ...this.state, ui: <StickiesConatiner logOutCb={() => {
          this.setState({ ...this.state, ui: <LoginPage /> })
        }} />
      })
    }} />,
  }

  render() {
    return (
      <div className="App">
        {this.state.ui}
      </div>
    );
  }
}
export default App;

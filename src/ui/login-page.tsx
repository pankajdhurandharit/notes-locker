import React from 'react';
import 'react-toastify/dist/ReactToastify.css';
import ReactSignupLoginComponent from 'react-signup-login-component';
import { toast, ToastContainer } from 'react-toastify';
import { signUpData, loginData } from './datamodal';
import { addDataInDB, getDataFromDb } from './utils';

export interface IProps {
    loginCB?: () => void;
}

export interface IState {
    seq: number
}

class LoginPage extends React.Component<IProps, IState> {
    state: IState = {
        seq: 0,
    }

    signupWasClickedCallback = async (data: signUpData) => {
        const mail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!mail.test(data.username)) {
            toast.warn("enter valid mail id", {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else if (data.password !== data.passwordConfirmation) {
            toast.warn("Password and confirm password does not match", {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            //DB CALL FOR SAVE DATA
            const usersCollectionsDataArr = await getDataFromDb(`users/${data.username}/users`);
            const matchedObject = usersCollectionsDataArr.find((val) => val.username === data.username);
            if (matchedObject === undefined) {
                addDataInDB(`users/${data.username}/users`, { username: data.username, password: data.password });
            } else {
                toast.info("User Already Exist", {
                    position: toast.POSITION.TOP_RIGHT
                });
            }
            this.setState({ ...this.state, seq: this.state.seq + 1 });
        }
    }

    loginWasClickedCallback = async (data: loginData) => {
        if (!data.username || !data.password) {
            toast.warn("Please enter Email address and password", {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            const usersCollectionsDataArr = await getDataFromDb(`users/${data.username}/users`);
            const matchedObject = usersCollectionsDataArr.find((val) => val.username === data.username);
            if (matchedObject === undefined) {
                toast.warn("User not registered", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            } else {
                this.props.loginCB!();
            }
        }
    };

    render() {
        return (
            <div>
                <ReactSignupLoginComponent
                    key={this.state.seq}
                    styles={{
                        flipper: { transition: '0.1s' },
                        signup: {
                            recoverPasswordButton: { dispaly: 'none' }
                        },
                        login: {
                            recoverPasswordButton: { display: 'none' },
                        },

                    }}
                    title="Create Notes"
                    handleSignup={async (signUpData: signUpData) => { await this.signupWasClickedCallback(signUpData) }}
                    handleLogin={(loginData: loginData) => { this.loginWasClickedCallback(loginData) }}
                    handleRecoverPassword={(data: any) => { console.log("==handle Recover Password====", data) }}
                    usernameCustomLabel="Email address"
                    submitLoginCustomLabel="Log In"
                />
                <ToastContainer />
            </div>
        );
    }
}

export default LoginPage;
export type signUpData = {
    id?: string
    password: string
    passwordConfirmation?: string
    username: string;
}
export type loginData = {
    password: string
    username: string
}
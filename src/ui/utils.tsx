import fireDb from "../index";
import { signUpData } from "./datamodal";
import 'react-toastify/dist/ReactToastify.css'
import { toast } from 'react-toastify';
/**
 * Return Collection data in Array
 * @param collectionName 
 * @returns 
 */
export const getDataFromDb = async (collectionName: string) => {

    const db = fireDb;
    let dataArray: any[] = [];
    const querySnapshot = await db.collection(collectionName).get();
    querySnapshot.docs.map((doc) =>
        dataArray.push({ id: doc.id, ...doc.data() })
    );
    return dataArray;
};


/**
 * create a document in db
 * @param collectionName 
 * @param createObject 
 */
export const addDataInDB = (collectionName: string,createObject:signUpData) => {
    fireDb.collection(collectionName).add(createObject).then((val: any) => {  
        toast.success("Successfully Created",{
            position:toast.POSITION.BOTTOM_RIGHT
        })
    }).catch((res: any) => {
        console.error("====add data in Db===", res);
    });
}